# You will be given an array that would contain only 0, 1, and 2.
# Write a function to sort the given array.
#
# Expected Time complexity - O(N)
# Expected Space complexity - O(1)
# Hint – DNF Problem
# Sample input: [0, 1, 2, 0, 1, 2]
# Sample output: [0, 0, 1, 1, 2, 2]
# Sample input: [0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1]
# Sample output: [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 2, 2]


def sortArray(input):
    size = len(input)
    count0 = count1 = count2 = 0
    output = [0] * size

    # Count the number of 0s, 1s and 2s in the input array
    for i in range(size):
        if input[i] == 0:
            count0 += 1
        elif input[i] == 1:
            count1 += 1
        elif input[i] == 2:
            count2 += 1

    # Now load all 0s, 1s and 2s into the output array
    i = 0
    while (count0 > 0):
        output[i] = 0
        i += 1
        count0 -= 1

    while (count1 > 0):
        output[i] = 1
        i += 1
        count1 -= 1

    while (count2 > 0):
        output[i] = 2
        i += 1
        count2 -= 1

    return output


input1 = [0, 1, 2, 0, 1, 2]
output1 = sortArray(input1)
print("Input1 = ", input1)
print("Output1 = ", output1)

input2 = [0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1]
output2 = sortArray(input2)
print("Input2 = ", input2)
print("Output2 = ", output2)
