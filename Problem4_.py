# You will be given a 2D-Array or matrix that would contain only 0 and 1. Each 0 represents land and 1 represents river. A river consists of any number of 1 that are either horizontally or vertically adjacent. Write a function that returns an array of the sizes of all rivers represented in the input matrix. The sizes need not be in any order.
#
# Expected Time complexity – O(m*n), where m is width of matrix and n is height of matrix.
# Expected Space complexity – O(m*n), where m is width of matrix and n is height of matrix.
# Sample Input -
# [
# [1, 0, 0, 1, 0],
# [1, 0, 1, 0, 0],
# [0, 0, 1, 0, 1],
# [1, 0, 1, 0, 1],
# [1, 0, 1, 1, 0]
# ]
# sample output: [2, 1, 5, 2, 2]
