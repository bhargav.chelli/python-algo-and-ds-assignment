# You will be given a non-empty array of integers. The array can contain positive, negative or zero as integers. The integers may not be in sorted order.
# Write a program to return largest range of integers contained in the array.
# Note that the numbers don’t need to be sorted or adjacent in the input array to for the range.
#
# For instance [1, 7] represents the range 1, 2, 3, 4, 5, 6, 7
# Expected Time complexity - O(N)
# Expected Space complexity - O(1)
# Sample input: [1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6]
# Sample output: [0, 7] # The above array contains the integers between range 0 to 7.
# Sample input: [19, -1, 18, 17, 2, 10, 3, 12, 5, 16, 4, 11, 8, 7, 6, 15, 12, 12, 2, 1, 6, 13, 14]
# Sample output: [10, 19]

def largestRange(input):
    size = len(input)
    temp = input
    max_value = 0;
    output = [0, 0];

    for i in range(size):
        if (input[i] in temp):
            j = input[i];
            while (j in temp):
                j += 1;
            max_value = max(max_value, j - input[i])
    output[1] = j - 1
    output[0] = j - max_value
    return output


input1 = [1, 11, 3, 0, 15, 5, 2, 4, 10, 7, 12, 6]
output1 = largestRange(input1)
print("Input1 = ", input1)
print("Output1 = ", output1)


input2 = [19, -1, 18, 17, 2, 10, 3, 12, 5, 16, 4, 11, 8, 7, 6, 15, 12, 12, 2, 1, 6, 13, 14]
output2 = largestRange(input2)
print("Input2 = ", input2)
print("Output2 = ", output2)

