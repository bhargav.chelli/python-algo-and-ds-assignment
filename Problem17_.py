# A tandem bicycle is a bicycle that is operated by two people. Person A and B.
# The person that pedals faster, dictates the speed of bicycle.
# So, if person A pedals at speed 5 and person B pedals at speed 4 then the speed of bicycle will be 5.
# You are given two arrays of positive integers.
# One array containing the speeds of person wearing red shirt and another array containing
# the speed of person wearing the blue shirt. Your task is to the red shirt person with blue shirt
# person in such a way that the total speed of all the bicycle will be maximum.
#
# Expected Time complexity – O(nlogn)
# Expected Space complexity – O(1)
# Sample input: [5, 5, 3, 9, 2] and [3, 6, 7, 2, 1]
# Sample output: 32
# Sample input: [1, 2, 1, 9, 12, 3] and [3, 3, 4, 6, 1, 2]
# Sample output: 37
