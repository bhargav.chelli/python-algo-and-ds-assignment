# Levenshtein Distance. Write a function that takes two strings and returns the minimum number of
# operations that need to be performed on the first string, to obtain the second string.
# Operations are inserting a character, removing a character, or replacing a character.
#
# Hint: Dynamic Programming.
# Expected Time complexity – O(nm), where n is length of string1 and m is length of string2
# Expected Space complexity – O(min(n, m))
# Sample input: "abc" and "yabd"
# Sample output: 2
# Sample input: “biting” and “mitten”
# Sample output: 4


def editDistance(input1, input2, length1, length2):
    if length1 == 0:
        return length2

    if length2 == 0:
        return length1

    if input1[length1 - 1] == input2[length2 - 1]:
        return editDistance(input1, input2, length1 - 1, length2 - 1)

    pos = min(editDistance(input1, input2, length1, length2 - 1),
              editDistance(input1, input2, length1 - 1, length2),
              editDistance(input1, input2, length1 - 1, length2 - 1))
    return pos + 1


input1 = "abc"
input2 = "yabd"
print("\nInput1 = ", input1)
print("Input2 = ", input2)
output = editDistance(input1, input2, len(input1), len(input2))
print("Output = ", output)

input1 = "biting"
input2 = "mitten"
print("\nInput1 = ", input1)
print("Input2 = ", input2)
output = editDistance(input1, input2, len(input1), len(input2))
print("Output = ", output)
