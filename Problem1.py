# You will be given a non-empty array of integers. The array can contain positive, negative or zero as integers. The integers may not be in sorted order and may contain duplicates.
# Write a program to find out the maximum sum that can be obtained by summing up all the adjacent integers of a subarray.
#
# Expected Time complexity - O(N)
# Expected Space complexity - O(1)
# Hint – Kadane’s Algo
# Sample input: [-2, -3, 4, -1, -2, 1, 5, -3]
# Sample output: 7
# Sample Input: [-13, -3, -25, -20, -3, -16, -23, -12, -5, -22, -15, -4, -7]
# Sample output: -3


def maxSum(input):
    size = len(input)
    max_sum = input[0]
    max_sum1 = 0

    for i in range(0, size):
        max_sum1 = max_sum1 + input[i]
        if max_sum < max_sum1:
            max_sum = max_sum1
        if max_sum1 < 0:
            max_sum1 = 0
    return max_sum


input1 = [-2, -3, 4, -1, -2, 1, 5, -3]
output1 = maxSum(input1)
print("Input1 = ", input1)
print("Output1 = ", output1)

input2 = [-13, -3, -25, -20, -3, -16, -23, -12, -5, -22, -15, -4, -7]
output2 = maxSum(input2)
print("Input2 = ", input2)
print("Output2 = ", output2)
