# You will be given a non-empty array of positive integers. Each integer represents the amount of time
# that specific queries take to execute. Only one query can be executed at a time,
# and queries can be executed in any order. A query’s waiting time is defined as the
# amount of time that it must wait before its execution starts. In other words,
# if a query is executed second, then its waiting time is the duration of the first query,
# if the query is executed third then its waiting time is the sum of the durations of the first two
# queries. Write a function to return the minimum amount of total waiting time for all the queries.
# For example, if the queries of durations [1, 4, 5], then the total waiting time if the queries
# executed in the order [5, 1, 4] would be 0 + 5 + (5+1) = 11.
#
# Expected Time complexity – O(nlogn)
# Expected Space complexity – O(1)
# Sample input: [3, 2, 1, 2, 6]
# Sample output: 17
# Sample input: [1, 2, 4, 5, 2, 1]
# Sample output: 23
