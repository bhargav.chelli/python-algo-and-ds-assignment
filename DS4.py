# Implement a binary search tree with insert, remove, search and traversal operations

class Node:
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None


def preorder(root):
    if root:
        print(root.key),
        preorder(root.left)
        preorder(root.right)


def inorder(root):
    if root:
        inorder(root.left)
        print(root.key),
        inorder(root.right)


def postorder(root):
    if root:
        postorder(root.left)
        postorder(root.right)
        print(root.key),


def insert(node, key):
    if node is None:
        return Node(key)
    if key < node.key:
        node.left = insert(node.left, key)
    else:
        node.right = insert(node.right, key)
    return node


def minValueNode(node):
    current = node
    while (current.left is not None):
        current = current.left
    return current


def deleteNode(root, key):
    if root is None:
        return root
    if key < root.key:
        root.left = deleteNode(root.left, key)
    elif (key > root.key):
        root.right = deleteNode(root.right, key)
    else:
        if root.left is None:
            temp = root.right
            root = None
            return temp
        elif root.right is None:
            temp = root.left
            root = None
            return temp
        temp = minValueNode(root.right)
        root.key = temp.key
        root.right = deleteNode(root.right, temp.key)

    return root


root = None
root = insert(root, 10)
root = insert(root, 20)
root = insert(root, 30)
root = insert(root, 40)
root = insert(root, 50)

print("Preorder traversal is")
preorder(root)

print("\nInorder traversal is")
inorder(root)

print("\nPostorder traversal is")
postorder(root)

print("\nDelete 20")
root = deleteNode(root, 20)

print("\nPostorder traversal after deleting 20 is")
postorder(root)
