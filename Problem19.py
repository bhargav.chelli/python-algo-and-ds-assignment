# You are given a non-empty array of positive integers where each integer represents the maximum number
# of steps you can take forward in the array. For example, if the element at index 1 is 3,
# you can go from index 1 to index 2, 3 or 4. Write a function that returns the minimum number of
# jumps needed to reach the final index. Jumping from index i to index i + x is considered as one jump.
#
# Expected Time complexity – O(N)
# Expected Space complexity – O(1)
# Sample input: [3, 4, 2, 1, 2, 3, 7, 1, 1, 1, 3]
# Sample output: 4
# Sample input: [3, 4, 2, 1, 2, 3, 7, 1, 1, 1, 3, 2, 6, 2, 1, 1, 1, 1]
# Sample output: 5


def minJumps(input, start):
    size = len(input)

    if (start == size - 1):
        return 0
    if (input[start] == 0):
        return -1

    output = 999
    for i in range(start + 1, size):
        if (i < start + input[start] + 1):
            jumps = minJumps(input, i)
            if (output > jumps + 1):
                output = jumps + 1

    return output


input1 = [3, 4, 2, 1, 2, 3, 7, 1, 1, 1, 3]
output1 = minJumps(input1, 0)
print("Input1 = ", input1)
print("Output1 =", output1)

input2 = [3, 4, 2, 1, 2, 3, 7, 1, 1, 1, 3, 2, 6, 2, 1, 1, 1, 1]
output2 = minJumps(input2, 0)
print("Input2 = ", input2)
print("Output2 =", output2)
