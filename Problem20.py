# Write a function that would take the root node of a binary tree as an input and return the sum of each path.
#
# Expected Time complexity – O(N), where N is the number of nodes in the binary tree.
# Expected Space complexity – O(h)
# Sample Input:
# 1
# / \
# 2 3
# / \ / \
# 3 4 5 6
# \ /
# 7 8
# 1+2+3+7 = 13
# 1+2+4 = 7
# 1+3+5 = 9
# 1+3+6+8 = 18
# Sample output: [13, 7, 9, 18]


class Node:

    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None


def sumOfEachPath(node, subSum, output):
    temp = 0
    subSum = subSum + node.data

    if (node.left == None and node.right == None):
        output.append(subSum)

    if node.left is not None:
        temp = sumOfEachPath(node.left, subSum, output)
    if node.right is not None:
        temp = sumOfEachPath(node.right, subSum, output)

    return output


subSum = 0
output = []
input = Node(1)
input.left = Node(2)
input.left.left = Node(3)
input.left.right = Node(4)
input.left.left.right = Node(7)
input.right = Node(3)
input.right.left = Node(5)
input.right.right = Node(6)
input.right.right.left = Node(8)

output = sumOfEachPath(input, subSum, output)
print("Output = ", output)
