# Write a function that would take the root node of a binary search tree and a positive
# integer k as input, and returns the kth largest integer contained in the bst.
#
# Expected Time complexity – O(h+k), where h is the number height of the tree.
# Expected Space complexity – O(h)
# Sample input:
#     15
#    /    \
#   5       20
# /  \     /  \
# 2   5   17 27
# / \
# 1 3
# K = 3
# Sample output: 17


class newNode:

    def __init__(self, data):
        self.key = data
        self.left = None
        self.right = None


def kthLargestElement(root, k, c):
    ouput = 0
    if root == None or c[0] >= k:
        return

    kthLargestElement(root.right, k, c)
    c[0] += 1
    if c[0] == k:
        print("Largest element is", root.key)
        return

    kthLargestElement(root.left, k, c)


root = newNode(15)
root.left = newNode(5)
root.right = newNode(20)
root.left.left = newNode(2)
root.left.right = newNode(5)
root.left.left.left = newNode(1)
root.left.left.right = newNode(3)
root.right.left = newNode(17)
root.right.right = newNode(27)

k = 3
output = kthLargestElement(root, k, [0])
