# Imagine a stack of plates. If the stack gets too high, it might topple.
# Therefore, in real life, we would likely start a new stack when the previous stack exceeds some threshold.
# Implement a stack of stacks that mimics the above scenario.
# Stack of stacks should be composed of several stacks and should create a new stack once the
# previous one exceeds capacity. stack_of_stack.push() and pop() should behave identically to a single stack,
# i.e. pop/push should return the same value as it would if there were just a single stack.