# You will be given a 2D-Array or matrix containing integers that could be positive, negative or 0.
# Write a function that returns the minimum number of passes required to convert all negative integers
# to positive integers. A negative integer in the matrix can only be converted to a positive integer
# if one or more of its adjacent elements is positive. An adjacent element is an element that is to
# the left, right, above or below the current element in the matrix. The 0 value is neither negative
# nor positive, so a 0 cannot convert its adjacent negative integer to positive integer.
#
# Expected Time complexity – O(m*n), where m is width of matrix and n is height of matrix.
# Expected Space complexity – O(m*n), where m is width of matrix and n is height of matrix.
# Sample input: [
# [0, -1, -3, 2, 0],
# [1, -2, -5, -1, -3],
# [3, 0, 0, -4, -1]
# ]
# Sample output: 3
