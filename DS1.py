# Implement a stack with push, pop, isemtpy, and peek operations.

from sys import maxsize

def createStack():
    stack = []
    return stack

def isEmpty(stack):
    return len(stack) == 0

def push(stack, item):
    stack.append(item)
    return item

def pop(stack):
    if (isEmpty(stack)):
        return "Nothing"
    return stack.pop()

def peek(stack):
    if (isEmpty(stack)):
        return "Nothing"
    return stack[len(stack) - 1]

stack = createStack()

pushed = push(stack, str(10))
print("Pushed to stack - ", pushed)

pushed = push(stack, str(20))
print("Pushed to stack - ", pushed)

popped = pop(stack)
print("Popped from stack - ", pushed)

peek = peek(stack)
print("Peek of the Stack - ", peek)

IsEmpty = isEmpty(stack)
print("Is Stack Empty? - ", IsEmpty)
