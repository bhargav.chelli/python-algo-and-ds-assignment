# Write a function that would take the root node of a binary tree as an input and return the sum of each level.
#
# Expected Time complexity – O(N), where N is the number of nodes in the binary tree.
# Expected Space complexity – O(N)
# Sample Input:
#         1 		    Level sum 1
#      /     \
#    2         3	    Level sum 2 + 3 = 5
#  /   \      /  \
# 3    4     5    6 	level sum 3 + 4 + 5 + 6 = 18
# \               /
#  7             8 	    level sum 7 + 8 = 15
#
# Sample output: 1, 5, 18, 15

class newNode:

    def __init__(self, data):
        self.data = data;
        self.left = None;
        self.right = None;


def sumAtNthLevel(root, N):
    if (root == None):
        return 0;

    temp = [];
    temp.append(root);
    level = 0;
    sum = 0;
    flag = 0;

    while (len(temp) != 0):
        size = len(temp);

        while (size != 0):
            size -= 1;
            temp1 = temp[0];
            temp.pop(0);

            if (level == N):
                flag = 1;
                sum += temp1.data;

            else:
                if (temp1.left):
                    temp.append(temp1.left);
                if (temp1.right):
                    temp.append(temp1.right);
        level += 1;
        if (flag == 1):
            break;

    return sum;


root = newNode(1)
root.left = newNode(2)
root.right = newNode(3)
root.left.left = newNode(3)
root.left.right = newNode(4)
root.left.left.right = newNode(7)
root.right.left = newNode(5)
root.right.right = newNode(6)
root.right.right.left = newNode(8)
output = [0] * 4

for N in range(0, 4):
    result = sumAtNthLevel(root, N)
    output[N] = result
print(output)
