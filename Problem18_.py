# A set of cities are connected in a circle, in clockwise direction.
# Each city has a fuel station that provides certain litres fuel, and each city is
# some distance away from the next city.
# You have a vehicle that can drive certain number of km per litres of fuel.
# Your goal is to pick the starting city in such a way that you fill – refill the fuel
# and drive to next city, until you return to the starting city with 0 or more litres of fuel left.
# You will be given an array of distances such that city i is distances[i] away from city i+1.
# You will be given an array of fuel available at each city, i.e. fuel[i] is fuel available at city i.
#
# Expected Time complexity – O(N)
# Expected Space complexity – O(1)
# Sample input:
# distances = [5, 25, 15, 10, 15]
# fuels = [1, 2, 1, 0, 3]
# mileage = 10
# Sample output: 4
# Sample input:
# distance = [30, 40, 10, 10, 17, 13, 50, 30, 10, 40]
# fuels = [1, 2, 0, 1, 1, 0, 3, 1, 0, 1]
# mileage = 25
# Sample output: 1
