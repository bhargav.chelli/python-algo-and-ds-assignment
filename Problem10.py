# Write a function that takes in an array of positive integers and returns the maximum sum of the
# nonadjacent elements in the array. If the array is empty, then return 0.
#
# Expected Time complexity – O(N)
# Expected Space complexity – O(1)
# Sample input - [75, 105, 120, 75, 90, 135]
# Sample output – 330 # 75 + 120 + 135 = 330
# Sample input - [7, 10, 12, 7, 9, 14]
# Sample output – 33 # 7 + 12 + 14 = 33

def maxSum(input):
    max_sum_include = 0
    max_sum_exclude = 0

    for i in input:
        new_max_sum_exclude = max(max_sum_exclude, max_sum_include)
        max_sum_include = max_sum_exclude + i
        max_sum_exclude = new_max_sum_exclude
    return max(max_sum_exclude, max_sum_include)


input1 = [75, 105, 120, 75, 90, 135]
output1 = maxSum(input1)
print("Input1 = ", input1)
print("Output1 = ", output1)

input2 = [7, 10, 12, 7, 9, 14]
output2 = maxSum(input2)
print("Input2 = ", input2)
print("Output2 = ", output2)
