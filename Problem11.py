# You will be given a 2D-Array or matrix that would contain only 0 and 1. Each 0 represents water
# and 1 represents land. An island is defined as any number of 1s that are horizontally or
# vertically adjacent and that don’t touch the borders. In other words, a group of horizontally
# or vertically adjacent 1s isn’t an island if any of those 1s are in the first row, last row,
# first column, last column.
#
# Expected Time complexity – O(m*n), where m is width of matrix and n is height of matrix.
# Expected Space complexity – O(m*n), where m is width of matrix and n is height of matrix.
# Sample input - [
# [1, 0, 0, 0, 0, 0],
# [0, 1, 0, 1, 1, 1],
# [0, 0, 1, 0, 1, 0],
# [1, 1, 0, 0, 1, 0],
# [1, 0, 1, 1, 0, 0],
# [1, 0, 0, 0, 0, 1]
# ]
# Sample output - [
# [1, 0, 0, 0, 0, 0],
# [0, 0, 0, 1, 1, 1],
# [0, 0, 0, 0, 1, 0],
# [1, 1, 0, 0, 1, 0],
# [1, 0, 0, 0, 0, 0],
# [1, 0, 0, 0, 0, 1]
# ]


def closedIsland(input, m, n):
    output = input
    visited = [[0 for i in range(m)]
               for j in range(n)]

    for i in range(n):
        for j in range(m):

            # Traverse Corners
            if ((i * j == 0 or i == n - 1 or j == m - 1) and
                    input[i][j] == 1 and visited[i][j] == 0):
                dfs(input, visited, i, j, m, n)

    for i in range(n):
        for j in range(m):

            # Replace 1 with 0 for any island
            if (visited[i][j] == 0 and input[i][j] == 1):
                output[i][j] = 0
                dfs(input, visited, i, j, m, n)
    return output


def dfs(input, visited, x, y, m, n):
    if (x < 0 or y < 0 or x >= n or y >= m or
            visited[x][y] == True or
            input[x][y] == 0):
        return

    visited[x][y] = True

    # Traverse to all adjacent elements
    dfs(input, visited, x + 1, y, m, n);
    dfs(input, visited, x, y + 1, m, n);
    dfs(input, visited, x - 1, y, m, n);
    dfs(input, visited, x, y - 1, m, n);


M = 6
N = 6
input = [[1, 0, 0, 0, 0, 0],
         [0, 1, 0, 1, 1, 1],
         [0, 0, 1, 0, 1, 0],
         [1, 1, 0, 0, 1, 0],
         [1, 0, 1, 1, 0, 0],
         [1, 0, 0, 0, 0, 1]]
print("Input  = ", input)
output = closedIsland(input, M, N)
print("Output = ", output)
