# You will be given a non-empty array of integers. The array can contain positive, negative or zero as integers. The integers may not be in sorted order and may contain duplicates.
# Write a program that returns a new array containing, at each index, the next element in the input array that’s greater than the element at that index in the input array. In other words, the function should return a new array where output_array[i] is the next element in the input_array thats greater than the input_array[i]. If there is no such next greater element for a particular index, the value at that index in the output array should be -1. For example, array = [1, 2], your function should return [2, -1]. Additionally, the function should treat the input array as circular array. It means for array = [0, 0, 5, 0, 0, 3, 0, 0], the next greater element after 3 is 5 since the array is circular.
#
# Expected Time complexity - O(N)
# Expected Space complexity - O(N)
# Sample input: [2, 5, -3, -4, 6, 7, 2]
# Sample output: [5, 6, 6, 6, 7, -1, 5]
# Sample input: [7, 6, 5, 4, 3, 2, 1]
# Sample output: [-1, 7, 7, 7, 7, 7, 7]


def nextLarge(input):
    size = len(input)
    output = [-1] * size
    temp1 = []

    for i in range(size):
        while temp1 and temp1[-1].get("value") < input[i]:
            temp2 = temp1.pop()
            output[temp2.get("ind")] = input[i]
        temp1.append({"value": input[i], "ind": i})

    while temp1:
        temp2 = temp1.pop()
        output[temp2.get("ind")] = -1
    return output


input1 = [2, 5, -3, -4, 6, 7, 2]
output1 = nextLarge(input1)
print("Input1 = ", input1)
print("Output1 = ", output1)

input2 = [7, 6, 5, 4, 3, 2, 1]
output2 = nextLarge(input2)
print("Input2 = ", input2)
print("Output2 = ", output2)
