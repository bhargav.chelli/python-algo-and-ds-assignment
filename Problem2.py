
# You will be given an array having at least 2 integers. The array can contain positive, negative or zero as integers. The integers may not be in sorted order and may contain duplicates.
# Write a program to return the start and end index of the smallest subarray(by size) in the input array, that needs to be sorted in place to make the input array completely sorted. If the input array is already sorted the function should return [-1, -1]
#
# Expected Time complexity - O(N)
# Expected Space complexity - O(1)
# Hint – Subarray Sort
# Sample input: [1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19]
# Sample output: [3, 9]
# Sample input: [4, 8, 7, 12, 11, 9, -1, 3, 9, 16, -15, 51, 7]
# Sample output: [0, 12]


def unsorted(input):
    size = len(input)
    start_pos = 0
    end_pos = size - 1
    output = [0, 0]

    for start_pos in range(0, end_pos):
        if input[start_pos] > input[start_pos + 1]:
            break

    # Already sorted?
    if start_pos == end_pos - 1:
        return [-1, -1]

    for end_pos in range(end_pos, -1, -1):
        if input[end_pos] < input[end_pos - 1]:
            break

    max = min = input[start_pos]
    for i in range(start_pos + 1, end_pos + 1):
        if input[i] > max:
            max = input[i]
        if input[i] < min:
            min = input[i]

    for i in range(start_pos):
        if input[i] > min:
            start_pos = i
            break

    i = size - 1
    while i >= end_pos + 1:
        if input[i] < max:
            end_pos = i
            break
        i -= 1

    output[0] = start_pos
    output[1] = end_pos
    return output


input1 = [1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19]
output1 = unsorted(input1)
print("Input1 = ", input1)
print("Output1 = ", output1)

input2 = [4, 8, 7, 12, 11, 9, -1, 3, 9, 16, -15, 51, 7]
output2 = unsorted(input2)
print("Input2 = ", input2)
print("Output2 = ", output2)

