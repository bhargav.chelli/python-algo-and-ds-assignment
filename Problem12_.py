# You are given a list (adjacency list representation of graph) of edges representing a
# non-weighted, directed graph with at least one node. Write a function that returns Boolean
# value representing whether the given graph contains any cycle or not.
# A cycle is defined as any number of vertices that are connected in a closed chain.
# Another way to define a cycle is when the first vertex is the same as last vertex.
#
# Expected Time complexity – O(v+e) # v is number of nodes (vertices), e is number of edges
# Expected Space complexity – O(v)
# Sample input: [
# [1, 3],
# [2, 3, 4],
# [0],
# [],
# [2, 5],
# []
# ]
# Sample output: True
# Sample input: [
# [1],
# [2, 3, 4, 5, 6, 7],
# [],
# [2, 7],
# [5],
# [],
# [4],
# []
# ]
# Sample output: False
