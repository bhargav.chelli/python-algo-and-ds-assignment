# You will be given a non-empty array of integers. The array can contain positive, negative or zero as integers. The integers may not be in sorted order and may contain duplicates.
# Write a program to return the array of same length where each element in the output array is equal to the product of every other number in the input array. So, it means that the value at output[i] will be equal to the product of every number in the input array other than input[i].
#
# Expected Time complexity - O(N)
# Expected Space complexity - O(N)
# Sample input: [5, 1, 4, 2]
# Sample output: [8, 40, 10, 20] # output[2] is input[0] * input[1] * input[3] i.e. 5*1*2 = 10


def productArray(input):
    size = len(input)
    left = [1] * size
    right = [1] * size
    product = [1] * size

    for i in range(1, size):
        left[i] = input[i - 1] * left[i - 1]

    for i in range(size - 2, -1, -1):
        right[i] = input[i + 1] * right[i + 1]

    for i in range(size):
        product[i] = left[i] * right[i]

    return product


input1 = [5, 1, 4, 2]
output1 = productArray(input1)
print("Input1 = ", input1)
print("Output1 =", output1)
